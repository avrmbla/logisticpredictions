## Formålet med dette script er at undersøge, hvor Hosmer og Lemeshow beregner estimerede sandsynligheder

source("funk.R")
# Installered nødvendige pakker
libs <- c("aplore3")
for(l in libs){
  if(!require(aplore3)){
    install.packages(l)
    stop("Re-run script after installation")
  }
  library(l,character.only = TRUE)
}

# Ser på data
print(head(burn1000))


covs <- c("tbsa","inh_inj","age","gender","flame","race")

# Laver regression
mod <- glm(formula = death ~ tbsa + inh_inj + age + gender + flame + 
    race, family = binomial, data = burn1000)

print(summary(mod))


# Make prediction based on median value
medians <- sapply(covs,function(x){
                    if(is.factor(burn1000[,x])){
                      median(as.numeric(burn1000[,x]))
                    } else {
                      median(burn1000[,x])
                    }
    })


print(medians)

newdata <- data.frame(t(medians))


#probabilities <- predict(mod,newdata,type = "response")
#print(probabilities)
predictedTerms <- predict(mod,type = "terms")
cat("Dette er head(predictedTerms))\n")
print(head(predictedTerms))


predicted <- predict(mod)
#print(head(predicted))
cat("Blot median af predicted, formentlig g(x)\n")
print(median(predicted))

# Now we subtract the rest
TBSApredicted <- getCoef(mod,"tbsa") * as.numeric(burn1000[,"tbsa"])
cat("Dette er head(TBSApredicted))\n")
print(head(TBSApredicted))
INH_INJ <- getCoef(mod,"inh_injYes") * as.numeric(burn1000[,"inh_inj"])


adjusted <- predicted - (TBSApredicted + INH_INJ)

cat("Ser på adjusted\n")
print(median(adjusted))

#print(median(predicted) - (median(TBSApredicted) + median(INH_INJ)))

print(summary(mod)$coefficients[,1])

# Forsøger at udregne det for de andre variable
const <- rep(summary(mod)$coefficients["(Intercept)",1],NROW(burn1000))
AGEpredicted <- summary(mod)$coefficients["age",1] * as.numeric(burn1000[,"age"])
GENDERpredicted <- summary(mod)$coefficients["genderMale",1] * as.numeric(burn1000[,"gender"])
FLAMEpredicted <- summary(mod)$coefficients["flameYes",1]* as.numeric(burn1000[,"flame"])
RACEpredicted <- summary(mod)$coefficients["raceWhite",1] * as.numeric(burn1000[,"race"])

gm <- const + AGEpredicted + GENDERpredicted + FLAMEpredicted + RACEpredicted - (TBSApredicted + INH_INJ)
print(median(gm))









